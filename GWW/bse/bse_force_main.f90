! Modified by Joshua Elliott November 2020 as JDE

program bse_punch

use io_global,          ONLY : stdout, ionode, ionode_id
use io_files,           ONLY : psfile, pseudo_dir,diropn
use io_files,           ONLY : prefix,tmp_dir,iunwfc
use mp_world,           ONLY : mpime
use mp_pools,           ONLY : kunit
USE wvfct,              ONLY : nbnd, et, npwx, btype
USE gvecw,              ONLY : ecutwfc
USE gvecs,              ONLY : doublegrid
use pwcom
USE wavefunctions,      ONLY : evc
use mp,                 ONLY : mp_bcast
USE mp_world,           ONLY : world_comm
USE fft_base,           ONLY : dfftp
use scf,                ONLY : vrs, vltot, v, kedtau
USE fft_custom_gwl
use bse_basic_structures
use exciton
USE constants,          ONLY : RYTOEV
USE mp,                 ONLY : mp_barrier
USE qpe_exc,            ONLY : qpc
use bse_wannier,        ONLY : num_nbndv,&
           l_truncated_coulomb,&
           truncation_radius, &
           dual_bse,&
           l_verbose,&
           scissor,&
           l_tdhf,l_fullbse,l_lf,l_rpa, qpe_imin, qpe_imax,&
           l_scissor
USE direct_www
USE  force_basic_structures
USE check_stop, ONLY : check_stop_init, check_stop_now
USE control_flags,        ONLY : max_cg_iter, david, nmix

implicit none
INTEGER, EXTERNAL :: find_free_unit

integer :: i, kunittmp, ios, is
CHARACTER(LEN=256), EXTERNAL :: trimcheck
CHARACTER(LEN=256) :: outdir
character(len=200) :: pp_file
logical ::  uspp_spsi, ascii, single_file, raw


type(v_state) :: vstate
type(v_state_r) :: vstate_r
type(c_state)   :: cstate
type(c_state)   :: wcstate
type(exc) :: a_exc
type(exc) :: b_exc
!type(exc) :: a_excdiago,a_exchange 
!type(exc):: a_dirv
!type(exc):: a_dirw
!type(exc):: a_rot
type(fft_cus) :: fc
TYPE(v_state_der) :: psi1!derivative of wavefunctions


logical exst
integer iuv


real(kind=DP) :: sdeig



NAMELIST /inputbse/ prefix,num_nbndv,dual_bse,outdir,l_truncated_coulomb,&
                    truncation_radius,  l_verbose, dual_bse,&
                    l_fullbse,l_tdhf,l_lf,l_rpa, qpe_imin, qpe_imax,&
                    l_scissor

            
call start_bse( )
call start_clock('bse_main')

david=4
!
!   set default values for variables in namelist
!
prefix='export'
 CALL get_environment_variable( 'ESPRESSO_TMPDIR', outdir )	
IF ( TRIM( outdir ) == ' ' ) outdir = './'
  pp_file= ' '
  uspp_spsi = .FALSE.
  ascii = .FALSE.
  single_file = .FALSE.
  raw = .FALSE.



num_nbndv(1:2) = 1
l_truncated_coulomb = .false.
truncation_radius = 10.d0
dual_bse=1.d0
l_verbose=.false.
l_fullbse=.true.
l_tdhf=.false.
l_lf=.false.
l_rpa=.false.
qpe_imin=1
qpe_imax=1
l_scissor=.true.
!
!    Reading input file
!
IF ( ionode ) THEN
      !
      CALL input_from_file ( )
      !
      READ(5,inputbse,IOSTAT=ios)
      !
!      call read_namelists( 'PW4GWW' )
      !
      IF (ios /= 0) CALL errore ('pw4gww', 'reading inputbse namelist', ABS(ios) )
      scissor=scissor/RYTOEV
ENDIF


!-------------------------------------------------------------------------
! ... Broadcasting variables
!------------------------------------------------------------------------

   

  tmp_dir = trimcheck( outdir )
  CALL mp_bcast( tmp_dir, ionode_id, world_comm )
  CALL mp_bcast( prefix, ionode_id , world_comm)
  CALL mp_bcast( num_nbndv,     ionode_id , world_comm)
  CALL mp_bcast(l_truncated_coulomb, ionode_id, world_comm)
  CALL mp_bcast(truncation_radius, ionode_id, world_comm)
  CALL mp_bcast(dual_bse, ionode_id, world_comm)
  CALL mp_bcast( l_fullbse, ionode_id, world_comm ) 
  CALL mp_bcast( l_tdhf, ionode_id, world_comm ) 
  CALL mp_bcast( l_lf, ionode_id, world_comm ) 
  CALL mp_bcast( l_rpa, ionode_id, world_comm ) 
  CALL mp_bcast( l_scissor, ionode_id, world_comm)
  CALL mp_bcast( qpe_imin, ionode_id, world_comm)
  CALL mp_bcast( qpe_imax, ionode_id, world_comm)

  call read_file 
! after read_file everything is known

#if defined __MPI
  kunittmp = kunit
#else
  kunittmp = 1
#endif

  call openfil_bse

  call read_export(pp_file,kunittmp,uspp_spsi, ascii, single_file, raw)
  call summary()
  !call init_run()
  allocate(btype(nbnd,nspin))
  nmix=10
  
  call print_bseinfo()
  call check_stop_init()
  call setup()
  CALL set_vrs(vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid )

  if(l_verbose) write(stdout,*) 'To check, we print the KS eigenvalues:'
  FLUSH( stdout )
  !
  CALL print_ks_energies()

! inizialize dual grid once for all
  fc%dual_t=dual_bse
  fc%ecutt=ecutwfc
  call initialize_fft_custom(fc)

!!!JUST DEBUG
  

  write(stdout,*) 'CALL MAKE_DERIVATIVE'
  call  make_derivative(1,1,0.01, vstate,psi1 )


  
  FLUSH( stdout )
  deallocate(btype)
  call stop_pp

  stop
end program bse_punch





